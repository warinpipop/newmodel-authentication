const mongoose = require('mongoose');
const config = require('./config')
mongoose.connect(config.mongoUrl,
{ useNewUrlParser: true},

)

mongoose.connection.on('connected',(err)=>{
    if(!err)
    console.log('MongoDB connection '+config.mongoUrl);
    else
    console.log('Error in DB connection : ' + JSON.stringify(err, undefined, 2));
})


process.on("SIGINT", function(){
    mongoose.connection.close(function(){
        console.log('mongoose close')
        process.exit(0);
    })
})
module.exports = mongoose;