process.env.NODE_ENV = process.env.NODE_ENV  || 'development';
require('./config/mongoose')
const express = require('express')
const bodyParser = require('body-parser')

const usermodel = require('./controller/User.controller')
const app = express();
PORT = 4000;

app.use(bodyParser.json())
app.use(bodyParser.urlencoded())
app.use(usermodel)
app.listen(PORT,()=>{
console.log("locahost:"+PORT+"!")})

