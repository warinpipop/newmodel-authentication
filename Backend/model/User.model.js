const mongoose = require('mongoose')
var Schema = mongoose.Schema;

var UserModel = mongoose.model('UserModel', new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    fristname: String,
    lastname: String,
    username: {type:String, 
        unique: true,
        trim: true,
        required: true
    },
    email: {
        type: String,
        index: true,
        required: true,
        match: /.+\@.+\.+/,
        required: true
    },
    password: {
        type: String,
        validate:[
            (password)=>{
                return password&&password.length >= 6;
            },
            "Password must be at least 6 chareter"
        ],
        required: true
    }
}))

module.exports = UserModel;